import csv
import subprocess
from pathlib import Path

import util


def run(app_conf, wav_ref_path, wav_degraded_path):
    output_csv = app_conf['VISCOL_OUTPUT_DIR'] + "/" + Path(util.path_leaf(wav_ref_path)).stem + "__" \
                 + Path(util.path_leaf(wav_degraded_path)).stem + "__viscol.csv"

    opts = [
        "--reference_file",
        wav_ref_path,
        "--degraded_file",
        wav_degraded_path,
        "--similarity_to_quality_model",
        app_conf[
          'VISCOL_MODEL'
        ],
        "--results_csv",
        output_csv
    ]

    subprocess.run([app_conf['VISCOL_EXECUTABLE'], *opts])

    return output_csv


def parse_single_result_csv(csv_path):

    with open(csv_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                line_count += 1
                return row[2]

    return None
