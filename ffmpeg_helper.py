import os.path
import subprocess
from pathlib import Path

import util


def extract_audio_aac(app_conf, video_path, bitrate):
    output_path = app_conf['AUDIO_OUTPUT_DIRS'] + "/" + Path(util.path_leaf(video_path)).stem + "__aac__{}".format(
        bitrate) + ".mp4"

    if os.path.exists(output_path):
        return output_path

    opts = [
        "-y",
        "-i",
        video_path,
        "-vn",
        "-c:a", "aac",
        "-b:a", bitrate,
        output_path
    ]

    subprocess.run(['ffmpeg', *opts])

    return output_path


def extract_audio_opus_cvbr_speech(app_conf, video_path, bitrate):
    output_path = app_conf['AUDIO_OUTPUT_DIRS'] + "/" + Path(util.path_leaf(video_path)).stem + "__opus__{}".format(
        bitrate) + ".mp4"

    if os.path.exists(output_path):
        return output_path

    opts = [
        "-y",
        "-i",
        video_path,
        "-vn",
        "-c:a", "libopus",
        "-b:a", bitrate,
        "-vbr", "constrained",
        "-application", "voip",
        "-strict", "-2",
        output_path
    ]

    subprocess.run(['ffmpeg', *opts])

    return output_path


def extract_audio_opus_cvbr_audio_mono(app_conf, video_path, bitrate):
    output_path = app_conf['AUDIO_OUTPUT_DIRS'] + "/" + Path(
        util.path_leaf(video_path)).stem + "__opus__mono__lowdelay{}".format(
        bitrate) + ".mp4"

    if os.path.exists(output_path):
        return output_path

    opts = [
        "-y",
        "-i",
        video_path,
        "-vn",
        "-c:a", "libopus",
        "-b:a", bitrate,
        "-vbr", "constrained",
        "-application", "lowdelay",
        "-strict", "-2",
        output_path
    ]

    subprocess.run(['ffmpeg', *opts])

    return output_path


def extract_audio_he_aac(app_conf, video_path, bitrate):
    output_path = app_conf['AUDIO_OUTPUT_DIRS'] + "/" + Path(util.path_leaf(video_path)).stem + "__he-aac__{}".format(
        bitrate) + ".mp4"

    if os.path.exists(output_path):
        return output_path

    opts = [
        "-y",
        "-i",
        video_path,
        "-vn",
        "-c:a", "libfdk_aac",
        "-b:a", bitrate,
        "-profile:a", "aac_he",
        output_path
    ]

    subprocess.run(['ffmpeg', *opts])

    return output_path


def extract_audio_he_aac_v2(app_conf, video_path, bitrate):
    output_path = app_conf['AUDIO_OUTPUT_DIRS'] + "/" + Path(
        util.path_leaf(video_path)).stem + "__he-aac-v2__{}".format(
        bitrate) + ".mp4"

    if os.path.exists(output_path):
        return output_path

    opts = [
        "-y",
        "-i",
        video_path,
        "-vn",
        "-c:a", "libfdk_aac",
        "-b:a", bitrate,
        "-profile:a", "aac_he_v2",
        output_path
    ]

    subprocess.run(['ffmpeg', *opts])

    return output_path


def to_wav(app_conf, audio_path, duration=10):
    output_path = app_conf['AUDIO_OUTPUT_DIRS'] + "/" + Path(util.path_leaf(audio_path)).stem + "__wav__" + ".wav"

    if os.path.exists(output_path):
        return output_path

    opts = [
        "-y",
        "-t",
        str(duration),
        "-i",
        audio_path,
        output_path
    ]

    subprocess.run(['ffmpeg', *opts])

    return output_path


def extract_audio_aac_copy(app_conf, video_path):
    output_path = app_conf['AUDIO_OUTPUT_DIRS'] + "/" + Path(util.path_leaf(video_path)).stem + "__aac__copy" + ".mp4"

    if os.path.exists(output_path):
        return output_path

    opts = [
        "-y",
        "-i",
        video_path,
        "-vn",
        "-acodec",
        "copy",
        output_path
    ]

    subprocess.run(['ffmpeg', *opts])

    return output_path
