# audio-benchmark

Script to do benchmark AAC vs Opus. This script use [VISQOL](https://github.com/google/visqol) to determine perceptual 
score.

There are 4 possible bitrates tested

```python
 bitrates = [
                "32K",
                "16K",
                "8K",
                "4K"
            ]
```

## Parameter Overview

### AAC
we use normal param as defined in [here](https://gitlab.com/ruangguru/source/-/blob/master/transcoder-worker-v2/service/inhouse/ffmpeg.go#L93)

### Opus

Parameter 

```python
 opts = [
        "-y",
        "-i",
        video_path,
        "-vn",
        "-c:a", "libopus",
        "-b:a", bitrate,
        "-vbr", "constrained",
        "-application", "voip",
        "-strict", "-2",
        output_path
```

[Refer to this](https://ffmpeg.org/ffmpeg-codecs.html#libopus-1)

## Testing overview

1. For each of videos, we extract audio files

- Reference (acodec copy) / no transcoding
- AAC with bitrates as defined above
- OPUS with bitrates as defined above
- WAV files for each of the following aac, opus and reference file. Note that wav file will be only included 10 seconds duration

2. For each of pair (AAC, Opus) in each of bitrate, we calculate VISCOL score
3. Save to CSV file

CSV file has following headers

- file_path
- bitrate
- reference_bytes
- aac_bytes
- opus_bytes
- aac_msqlo (score of visqol)
- opus_msqlo
- aac_reduction_size (in percent)
- opus_reduction_size (in percent)

## Usage

```shell
usage: main.py [-h] [-i INPUT_FILE]

Script to do audio benchmark between aac and opus codec

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE, --input-file INPUT_FILE
                        input file containing video filepaths
```

- input-file, file contains path of videos separated by `\n`

### Envar

- app.env

```text
AUDIO_OUTPUT_DIRS=audios
VISCOL_OUTPUT_DIR=viscol
VISCOL_EXECUTABLE=/mnt/e/go/src/github.com/google/visqol/bazel-bin/visqol
VISCOL_MODEL=/mnt/e/go/src/github.com/google/visqol/model/libsvm_nu_svr_model.txt
SUMMARY_OUTPUT_DIR=summary
```

Please make sure you have clone the [VISQOL](https://github.com/google/visqol) repository
to be able to fill `VISCOL_*` envar