import argparse
import csv
import os.path
import time
from pathlib import Path

from dotenv import dotenv_values

import ffmpeg_helper
import util
import viscol_helper

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Script to do audio benchmark between aac and opus codec')
    parser.add_argument("-i", "--input-file", metavar='INPUT_FILE', help='input file containing video filepaths')

    args = parser.parse_args()

    app_conf = dotenv_values("app.env")

    start = time.time()

    with open(args.input_file) as input_stream:

        while True:

            line = input_stream.readline()

            if not line:
                break

            video_path = line.strip()

            print("Processing video file {}".format(video_path))

            bitrates = [
                "32K",
                "24K",
                "16K",
                "8K",
                "4K"
            ]

            # extract the original audio
            # this line of code is to change from original audio extraction
            # to be aac 64K
            # reference_audio = ffmpeg_helper.extract_audio_aac(app_conf, video_path, '64K'
            #
            #                                                   )
            reference_audio = ffmpeg_helper.extract_audio_aac_copy(app_conf, video_path)
            reference_wav = ffmpeg_helper.to_wav(app_conf, reference_audio)
            reference_bytes = os.path.getsize(reference_audio)

            old_encoded = ffmpeg_helper.extract_audio_aac(app_conf, video_path, '64K')
            old_ref_bytes = os.path.getsize(old_encoded)
            old_ref_wav = ffmpeg_helper.to_wav(app_conf, old_encoded)

            old_msqlo_csv = viscol_helper.run(app_conf, reference_wav, old_ref_wav)
            old_msqlo = viscol_helper.parse_single_result_csv(old_msqlo_csv)

            headers = [
                "bitrate",
                "tested_source_file",
                "reference_bytes",
                "old_bytes",
                "resulting_aac_file",
                "aac_bytes",
                "resulting_opus_file",
                "opus_bytes",
                "resulting_opus_mono_file",
                "opus_mono_bytes",
                "resulting_he_aac_file",
                "he_aac_bytes",
                "resulting_he_aac_v2_file",
                "he_aac_v2_bytes",
                "aac_reduction_size_with_ref",
                "opus_reduction_size_with_ref",
                "opus_mono_reduction_size_with_ref",
                "he_aac_reduction_size_with_ref",
                "he_aac_v2_reduction_size_with_ref",
                "aac_reduction_size_with_old",
                "opus_reduction_size_with_old",
                "opus_mono_reduction_size_with_old",
                "he_aac_reduction_size_with_old",
                "he_aac_v2_reduction_size_with_old",
                "old_visqol",
                "aac_visqol",
                "opus_visqol",
                "opus_mono_visqol",
                "he_aac_visqol",
                "he_aac_v2_visqol"
            ]

            csv_data = []

            for bitrate in bitrates:
                aac_audio = ffmpeg_helper.extract_audio_aac(app_conf, video_path, bitrate)
                opus_audio = ffmpeg_helper.extract_audio_opus_cvbr_speech(app_conf, video_path, bitrate)
                opus_mono = ffmpeg_helper.extract_audio_opus_cvbr_audio_mono(app_conf, video_path, bitrate)
                he_aac_audio = ffmpeg_helper.extract_audio_he_aac(app_conf, video_path, bitrate)
                he_aac_v2_audio = ffmpeg_helper.extract_audio_he_aac_v2(app_conf, video_path, bitrate)

                # get file size
                aac_size_bytes = os.path.getsize(aac_audio)
                opus_size_bytes = os.path.getsize(opus_audio)
                opus_mono_size_bytes = os.path.getsize(opus_mono)
                he_aac_size_bytes = os.path.getsize(he_aac_audio)
                he_aac_v2_size_bytes = os.path.getsize(he_aac_v2_audio)

                aac_reduction_size = (reference_bytes - aac_size_bytes) / reference_bytes * 100
                opus_reduction_size = (reference_bytes - opus_size_bytes) / reference_bytes * 100
                opus_mono_reduction_size = (reference_bytes - opus_mono_size_bytes) / reference_bytes * 100
                he_aac_reduction_size = (reference_bytes - he_aac_size_bytes) / reference_bytes * 100
                he_aac_v2_reduction_size = (reference_bytes - he_aac_v2_size_bytes) / reference_bytes * 100

                aac_reduction_size_old = (old_ref_bytes - aac_size_bytes) / old_ref_bytes * 100
                opus_reduction_size_old = (old_ref_bytes - opus_size_bytes) / old_ref_bytes * 100
                opus_mono_reduction_size_old = (old_ref_bytes - opus_mono_size_bytes) / old_ref_bytes * 100
                he_aac_reduction_size_old = (old_ref_bytes - he_aac_size_bytes) / old_ref_bytes * 100
                he_aac_v2_reduction_size_old = (old_ref_bytes - he_aac_v2_size_bytes) / old_ref_bytes * 100

                row = [
                    bitrate,
                    video_path,
                    reference_bytes,
                    old_ref_bytes,
                    aac_audio,
                    aac_size_bytes,
                    opus_audio,
                    opus_size_bytes,
                    opus_mono,
                    opus_mono_size_bytes,
                    he_aac_audio,
                    he_aac_size_bytes,
                    he_aac_v2_audio,
                    he_aac_v2_size_bytes,
                    aac_reduction_size,
                    opus_reduction_size,
                    opus_mono_reduction_size,
                    he_aac_reduction_size,
                    he_aac_v2_reduction_size,
                    aac_reduction_size_old,
                    opus_reduction_size_old,
                    opus_mono_reduction_size_old,
                    he_aac_reduction_size_old,
                    he_aac_v2_reduction_size_old,
                    old_msqlo
                ]

                # convert to wav

                aac_wav = ffmpeg_helper.to_wav(app_conf, aac_audio)
                opus_wav = ffmpeg_helper.to_wav(app_conf, opus_audio)
                opus_mono_wav = ffmpeg_helper.to_wav(app_conf, opus_mono)
                he_aac_wav = ffmpeg_helper.to_wav(app_conf, he_aac_audio)
                he_aac_v2_wav = ffmpeg_helper.to_wav(app_conf, he_aac_v2_audio)

                # check viscol

                viscol_aac_csv = viscol_helper.run(app_conf, reference_wav, aac_wav)
                viscol_opus_csv = viscol_helper.run(app_conf, reference_wav, opus_wav)
                viscol_opus_mono_csv = viscol_helper.run(app_conf, reference_wav, opus_mono_wav)
                viscol_he_aac_csv = viscol_helper.run(app_conf, reference_wav, he_aac_wav)
                viscol_he_aac_v2_csv = viscol_helper.run(app_conf, reference_wav, he_aac_v2_wav)

                aac_msqlo = viscol_helper.parse_single_result_csv(viscol_aac_csv)
                opus_msqlo = viscol_helper.parse_single_result_csv(viscol_opus_csv)
                opus_mono_msqlo = viscol_helper.parse_single_result_csv(viscol_opus_mono_csv)
                he_aac_msqlo = viscol_helper.parse_single_result_csv(viscol_he_aac_csv)
                he_aac_v2_msqlo = viscol_helper.parse_single_result_csv(viscol_he_aac_v2_csv)

                row.append(aac_msqlo)
                row.append(opus_msqlo)
                row.append(opus_mono_msqlo)
                row.append(he_aac_msqlo)
                row.append(he_aac_v2_msqlo)

                csv_data.append(row)

            # write csv

            csv_out_file = app_conf['SUMMARY_OUTPUT_DIR'] + "/" + Path(util.path_leaf(video_path)).stem + ".csv"

            with open(csv_out_file, 'w', encoding='UTF-8', newline='') as csv_out:
                writer = csv.writer(csv_out)

                writer.writerow(headers)

                writer.writerows(csv_data)

    end = time.time()

    print("Script took {} seconds".format(end - start))